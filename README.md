Google Apps Agent
=================
Fenix invokes web services on this agent for provision, authentication and changing password.
## Dev

**Maven**
Tested on latest maven, version 3.0.4

**Eclipse**
Generate .project and .classpath files run
mvn eclipse:eclipse -DdownloadSources -DdownloadJavadocs

To revert your eclipse configuration and recreate it again use
mvn eclipse:clean eclipse:eclipse

**Configuration**
To configure the application copy the *src/main/resources/configuration.properties.sample* to *src/test/resources/configuration.properties* and change the file.

# Deployment
Generate jar
mvn clean package

Copy jars to artifact repository
mvn deploy
