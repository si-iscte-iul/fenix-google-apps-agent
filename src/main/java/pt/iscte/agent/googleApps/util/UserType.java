package pt.iscte.agent.googleApps.util;

public enum UserType {
    EMPLOYEE("iscte.pt", "Funcionarios"), STUDENT("iscte-iul.pt", "Alunos");

    private String domain;
    private String orgUnit;

    UserType(String domain, String orgUnit) {
        this.domain = domain;
        this.orgUnit = orgUnit;
    }

    public String getDomain() {
        return domain;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public static UserType fromOrgUnit(String orgUnitPath) {
        if (orgUnitPath.startsWith(EMPLOYEE.getOrgUnit())) {
            return EMPLOYEE;
        }
        if (orgUnitPath.startsWith(STUDENT.getOrgUnit())) {
            return STUDENT;
        }
        return null;
    }

    public static UserType fromUsername(String username) {
        final String domain = username.split("@")[1];
        if (domain.startsWith(EMPLOYEE.getDomain())) {
            return EMPLOYEE;
        }
        if (domain.startsWith(STUDENT.getDomain())) {
            return STUDENT;
        }
        return null;
    }
}
