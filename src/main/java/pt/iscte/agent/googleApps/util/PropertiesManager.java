package pt.iscte.agent.googleApps.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {

    /**
     * The interface that must be implemented by the class responsible to read the properties from a datasource.
     */
    public interface IPropertiesResolver {
        public String getProperty(String key);
    }

    /**
     * The default
     */
    public static class DefaultPropertiesResolver implements IPropertiesResolver {

        private Properties properties;

        public DefaultPropertiesResolver() {
            try {
                properties = new Properties();
                loadProperties(properties, "/configuration.properties");
            } catch (IOException e) {
                throw new RuntimeException("Unable to load properties files.", e);
            }
        }

        public DefaultPropertiesResolver(final Properties properties) {
            this.properties = properties;
        }

        public static Properties loadProperties(final String fileName) throws IOException {
            final Properties properties = new Properties();
            loadProperties(properties, fileName);
            return properties;
        }

        public static void loadProperties(final Properties properties, final String fileName) throws IOException {
            final InputStream inputStream = IPropertiesResolver.class.getResourceAsStream(fileName);
            if (inputStream != null) {
                properties.load(inputStream);
            }
        }

        @Override
        public String getProperty(String key) {
            return properties.getProperty(key);
        }

    }

    private static IPropertiesResolver resolverImpl;

    /**
     * Initialize the PropertiesManager class.
     * 
     * This method must be called before calling the getXYZ methods.
     * 
     * @param resolver The properties resolver class.
     */
    public static void init(final IPropertiesResolver resolver) {
        resolverImpl = resolver;
    }

    public static String getProperty(final String key) {
        return resolverImpl.getProperty(key);
    }

    public static boolean getBooleanProperty(final String key) {
        return Boolean.parseBoolean(resolverImpl.getProperty(key));
    }

    public static Integer getIntegerProperty(final String key) {
        return Integer.valueOf(resolverImpl.getProperty(key));
    }
}
