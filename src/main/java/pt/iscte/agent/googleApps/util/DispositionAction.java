package pt.iscte.agent.googleApps.util;

/**
 * The state that a message should be left in after it has been forwarded.
 * 
 * Acceptable values are:
 * "archive": Archive the message.
 * "dispositionUnspecified": Unspecified disposition.
 * "leaveInInbox": Leave the message in the INBOX.
 * "markRead": Leave the message in the INBOX and mark it as read.
 * "trash": Move the message to the TRASH.
 */
public enum DispositionAction {
    ARCHIVE("archive"), UNSPECIFIED("dispositionUnspecified"), KEEP("leaveInInbox"), MARK_AS_READ("markRead"), DELETE("trash");

    private String code;

    private DispositionAction(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
