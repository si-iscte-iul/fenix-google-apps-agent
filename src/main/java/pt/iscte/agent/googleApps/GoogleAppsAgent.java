package pt.iscte.agent.googleApps;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.model.User;
import com.google.gdata.util.AuthenticationException;

import pt.iscte.agent.googleApps.dto.ForwardDTO;
import pt.iscte.agent.googleApps.dto.GoogleAccountDTO;
import pt.iscte.agent.googleApps.dto.GoogleAccountForwardDTO;
import pt.iscte.agent.googleApps.dto.SendAsDTO;
import pt.iscte.agent.googleApps.service.AuthenticationService;
import pt.iscte.agent.googleApps.service.DirectoryService;
import pt.iscte.agent.googleApps.service.GmailService;
import pt.iscte.agent.googleApps.util.DispositionAction;
import pt.iscte.agent.googleApps.util.UserType;

public class GoogleAppsAgent implements IGoogleAppsAgent {

    private final Logger LOGGER;

    public GoogleAppsAgent() {
        LOGGER = Logger.getLogger(GoogleAppsAgent.class.getSimpleName());
    }

    /**
     * Logs the user into his or her Google Apps account.
     * 
     * WARNING: this method may fail if the user tries to log in with wrong
     * credentials more than a few times. Then Google will present us with
     * CAPTCHAs (security picture with letters which you have to copy down in
     * order to try to log in a second time) and they can't be handled
     * programmatically.
     * 
     * @param username
     *            User name (user\@domain.country)
     * @param password
     *            User password.
     * @return A boolean indicating if the user has successfully logged in.
     */
    public boolean authenticate(String username, String password) {
        try {
            AuthenticationService.checkCredentials(username, password);
            return true;
        } catch (AuthenticationException e) {
            LOGGER.info("Couldn't login in Google API as user '" + username + "': " + e);
        }

        return false;
    }

    /**
     * Changes the password for user in Google Apps.
     * 
     * @param username
     *            User name (user\@domain.country)
     * @param password
     *            User password.
     * @return A boolean indicating if changing of the password was successful.
     */
    public boolean changePassword(String username, String password) {
        try {
            DirectoryService.changePassword(username, password);

            return true;
        } catch (Exception e) {
            LOGGER.error("Error in changePassword for user: " + username, e);
        }

        return false;
    }

    /**
     * Checks if the user has a Google Apps account.
     * 
     * @param username
     *            User name (user\@domain.country)
     * @return A boolean indicating if the user has a Google Apps account.
     */
    public boolean isUserInGoogle(String username) {
        try {
            Directory directoryService = DirectoryService.getService();

            User user = directoryService.users().get(username).execute();
            if (user == null) {
                return false;
            }
            return true;
        } catch (GoogleJsonResponseException e) {
            if (e.getStatusCode() == 404) {
                LOGGER.debug("User '" + username + "' doesn't have a Google account: " + e);
            } else {
                LOGGER.error("Error in isUserInGoogle for user: " + username, e);
            }
        } catch (Exception e) {
            LOGGER.error("Error in isUserInGoogle for user: " + username, e);
        }
        return false;
    }

    /**
     * Creates/update a user in Google Apps.
     * 
     * @param data
     * @return true if the user was successfully created, false otherwise.
     */
    public boolean synchronizeUser(GoogleAccountDTO data) {
        try {
            performSynchronization(data);

            // if there are no exceptions then we assume that the user was
            // created/updated successfully.
            return true;
        } catch (IllegalArgumentException e) {
            LOGGER.error("Error in synchronizeUser for user: " + data.getUsername(), e);
            throw e;
        } catch (Exception e) {
            LOGGER.error("Error in synchronizeUser for user: " + data.getUsername(), e);
        }

        return false;
    }

    // ------------------------------------------------------------------------

    private static void performSynchronization(GoogleAccountDTO param) throws IOException {
        if (param.getUserType() != null) {
            throw new IllegalArgumentException("UserType cannot have data.");
        }
        if (param.getOrgUnitPath() != null) {
            throw new IllegalArgumentException("OrgUnitPath cannot have data.");
        }

        final UserType userType = UserType.fromUsername(param.getUsername());
        final String orgUnitPath = DirectoryService.buildOrgUnit(userType, param.getGmailBlocked());
        final GoogleAccountDTO dataNew = new GoogleAccountDTO(param, userType, orgUnitPath);

        User dataOld = DirectoryService.findUser(param.getUsername());

        if (dataOld != null) {
            boolean beforeGMailEnabled = !DirectoryService.isGmailBlocked(dataOld.getOrgUnitPath());
            boolean afterGMailEnabled = !DirectoryService.isGmailBlocked(orgUnitPath);

            // Update existing user
            dataOld = renameUserIfNeeded(dataOld, dataNew);
            syncAlias(dataOld, dataNew);

            // ----------------------------------------------------------------
            if (beforeGMailEnabled && !afterGMailEnabled) {
                syncForward(param, true);
                syncSendAs(param, true);
            }

            // The method below changes the OU so
            // it can put the user in a OU without GMail enabled 
            updatePersonalDataIfNeeded(dataOld, dataNew);

            if (afterGMailEnabled && !beforeGMailEnabled) {
                syncForward(param, true);
                syncSendAs(param, true);
            }
            // ----------------------------------------------------------------
        } else {
            // Create new user  OK
            createUser(dataNew);
            createAliases(param);

            if (!param.getGmailBlocked()) {
                syncForward(param, false);
                syncSendAs(param, false);
            }
        }
    }

    private static void createAliases(GoogleAccountDTO param) throws IOException {
        Set<String> aliases = getCorrectAlias(param);
        for (String alias : aliases) {
            DirectoryService.createAlias(param.getUsername(), alias);
        }
    }

    private static void createUser(GoogleAccountDTO dataNew) throws IOException {
        DirectoryService.createUser(dataNew);
    }

    // FENIX-10482 - Do not try to change forward anymore since
    // 1- The need to forward email to Neftis does not exist anymore
    // 2- If the GMail service is disable it will throw an Exception
    // 3- The email service is Office 365, so since it is received in Office 365 instead of GMail it it not forward
    private static void syncForward(GoogleAccountDTO param, boolean isUpdate)
            throws IllegalArgumentException, MalformedURLException, IOException {
        final String username = param.getUsername();
        GmailService service = GmailService.createGmailService(username);

        if (param.getGmailBlocked()) {
            // Forward to "@iul.pt"
            String forwardTo = username.split("@")[0] + "@iul.pt";

            ForwardDTO forwardDTO = new ForwardDTO(username, true, forwardTo, DispositionAction.DELETE.getCode());
            service.changeForwardingDTO(forwardDTO);
        } else {
            if (isUpdate) {
                // Remove forward to "@iul.pt" if active
                ForwardDTO forwardResultDTO = service.retrieveForwardingDTO(username);
                if (forwardResultDTO != null && forwardResultDTO.getForwardTo() != null
                        && forwardResultDTO.getForwardTo().endsWith("@iul.pt")) {
                    ForwardDTO forwardParamDTO = new ForwardDTO(username, false, null, null);
                    service.changeForwardingDTO(forwardParamDTO);
                }
            }
        }
    }

    private static void syncSendAs(GoogleAccountDTO param, boolean isUpdate)
            throws IllegalArgumentException, MalformedURLException, IOException {
        final String username = param.getUsername();
        GmailService service = GmailService.createGmailService(username);

        if (isUpdate) {
            String currentSendAsAddress = null;

            List<SendAsDTO> result = service.retrieveSendAsDTO(username);
            if (result != null) {
                for (SendAsDTO sendAsDTO : result) {
                    if (sendAsDTO.isDefault()) {
                        currentSendAsAddress = sendAsDTO.getAddress();
                    }
                }
            }

            if (currentSendAsAddress != null) {
                if (isExternalEmail(currentSendAsAddress) || currentSendAsAddress.equalsIgnoreCase(param.getMainAlias())) {
                    // External email or unchanged "send as" email address
                    return;
                }
            }
        }

        if (!username.equalsIgnoreCase(param.getMainAlias())) {
            SendAsDTO input = new SendAsDTO(username, param.getFirstName() + " " + param.getLastName(), param.getMainAlias(), "",
                    true, true);
            service.createSendAsDTO(input);
        }
    }

    private static boolean isExternalEmail(String email) {
        if (email.endsWith("@iscte.pt") || email.endsWith("@iscte-iul.pt")) {
            return false;
        }
        return true;
    }

    private static void updatePersonalDataIfNeeded(final User dataOld, GoogleAccountDTO dataNew) throws IOException {
        // Check if it needs to be updated
        if (!dataOld.getName().getGivenName().equals(dataNew.getFirstName())
                || !dataOld.getName().getFamilyName().equals(dataNew.getLastName())
                || !dataOld.getOrgUnitPath().equals(dataNew.getOrgUnitPath())) {
            // Update it
            DirectoryService.updateUserFullNameAndOrgUnitPath(dataNew.getUsername(), dataNew.getFirstName(),
                    dataNew.getLastName(), dataNew.getOrgUnitPath());
        }
    }

    private static User renameUserIfNeeded(final User dataOld, final GoogleAccountDTO dataNew) throws IOException {
        if (!dataOld.getPrimaryEmail().equals(dataNew.getUsername())) {
            // Rename user and return an updated view of the data with the created alias
            return DirectoryService.updateUsername(dataOld.getPrimaryEmail(), dataNew.getUsername());
        }

        return dataOld;
    }

    private static void syncAlias(User dataOld, GoogleAccountDTO dataNew) throws MalformedURLException, IOException {
        final String username = dataNew.getUsername();

        Set<String> correctAlias = getCorrectAlias(dataNew);

        List<String> allAlias = new ArrayList<String>();
        if (dataOld.getAliases() != null) {
            for (String alias : dataOld.getAliases()) {
                allAlias.add(alias);
            }
        }

        // Remove invalid alias
        List<String> invalidAliass = detectInvalidAlias(allAlias, correctAlias);
        DirectoryService.deleteAliases(username, invalidAliass);

        // Add needed alias
        List<String> newAliases = detectNewAlias(allAlias, correctAlias);
        DirectoryService.createAliases(username, newAliases);
    }

    private static Set<String> getCorrectAlias(GoogleAccountDTO data) {
        String username = data.getUsername();
        String usernameNoDomain = username.split("@")[0];

        String mainAlias = data.getMainAlias();
        String mainAliasNoDomain = mainAlias.split("@")[0];

        Set<String> correctAlias = new HashSet<String>();
        correctAlias.add(usernameNoDomain + "@iscte.pt");
        correctAlias.add(mainAliasNoDomain + "@iscte.pt");

        // Always add the iscte-iul.pt alias to ease the share.
        // This way the share does not fail even if the
        // user typed the wrong domain.
        correctAlias.add(usernameNoDomain + "@" + UserType.STUDENT.getDomain());
        correctAlias.add(mainAliasNoDomain + "@" + UserType.STUDENT.getDomain());

        correctAlias.remove(username);
        return correctAlias;
    }

    private static List<String> detectNewAlias(List<String> allAlias, Set<String> correctAlias) {
        List<String> newAlias = new ArrayList<String>();
        for (String alias : correctAlias) {
            if (!allAlias.contains(alias)) {
                newAlias.add(alias);
            }
        }

        return newAlias;
    }

    private static List<String> detectInvalidAlias(List<String> allAlias, Set<String> correctAlias) {
        List<String> toDeleteAlias = new ArrayList<String>();
        for (String alias : allAlias) {
            if (!correctAlias.contains(alias)) {
                toDeleteAlias.add(alias);
            }
        }

        return toDeleteAlias;
    }

    // ------------------------------------------------------------------------

    /**
     * Get an user google account information.
     * 
     * @param username
     *            (user\@domain.country)
     * @return user data.
     */
    public GoogleAccountDTO getUserInfo(String username) {
        try {
            GoogleAccountDTO result = getUserDetail(username);

            return result;
        } catch (GoogleJsonResponseException e) {
            System.out.println("e.getStatusCode(): " + e.getStatusCode());
            System.out.println("e.getStatusMessage(): " + e.getStatusMessage());
            System.out.println("e.getDetails().getCode(): " + e.getDetails().getCode());
            System.out.println();

            System.out.println("An error occurred: " + e);
        } catch (IOException e) {
            System.out.println("An error occurred: " + e);
        }

        return null;
    }

    private static GoogleAccountDTO getUserDetail(String username) throws IOException {
        User user = DirectoryService.findUser(username);

        if (user == null) {
            return null;
        }

        final String orgUnitPath = user.getOrgUnitPath();
        final UserType userType = UserType.fromOrgUnit(orgUnitPath);
        final Boolean gmailBlocked = DirectoryService.isGmailBlocked(orgUnitPath);

        GoogleAccountDTO result = new GoogleAccountDTO(user.getPrimaryEmail(), null, user.getName().getGivenName(),
                user.getName().getFamilyName(), null, gmailBlocked, userType, orgUnitPath);
        result.addAlias(user.getAliases());
        
        if (!gmailBlocked) {
            // If the OU has the GMail service disabled, then
            // we cannot call the API to get the forward settings,
            // since it gives the error "Mail service not enabled"
            try {
                GmailService service = GmailService.createGmailService(username);
                ForwardDTO forwardInfo = service.retrieveForwardingDTO(user.getPrimaryEmail());
                result.setForwardInfo(new GoogleAccountForwardDTO(forwardInfo));
            } catch (IllegalArgumentException e) {
                throw new RuntimeException(e);
            }
        }

        return result;
    }

    /**
     * Disables a user account.
     * 
     * @param username
     *            (user\@domain.country)
     * @return true if the account was successfully disabled, false otherwise.
     */
    public boolean disableAccount(String username) {
        try {
            DirectoryService.suspend(username, Boolean.TRUE);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error in disableAccount for user: " + username, e);
        }

        return false;
    }

    /**
     * Enables a user account.
     * 
     * @param username
     *            (user\@domain.country)
     * @return true if the account was successfully enabled, false otherwise.
     */
    public boolean enableAccount(String username) {
        try {
            DirectoryService.suspend(username, Boolean.FALSE);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error in enableAccount for user: " + username, e);
        }

        return false;
    }

    /**
     * Deletes a user account. You have to wait for 5 days before you can use
     * the user name once more.
     * 
     * @param username
     *            (user\@domain.country)
     * @return true if the account was successfully disabled, false otherwise.
     */
    public boolean deleteAccount(String username) {
        try {
            DirectoryService.delete(username);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error in deleteAccount for user: " + username, e);
        }

        return false;
    }

    /**
     * Synchronizes the "send as" email address.
     * 
     * @param data
     * @return true if the synchronization was successfully, false otherwise.
     */
    public boolean synchronizeSendAs(GoogleAccountDTO data) {
        try {
            syncSendAs(data, true);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error in synchronizeSendAs for user: " + data.getUsername(), e);
        }

        return false;
    }

}
