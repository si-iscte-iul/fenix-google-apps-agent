package pt.iscte.agent.googleApps.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.Arrays;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.gmail.GmailScopes;

import pt.iscte.agent.googleApps.util.PropertiesManager;

/**
 * Credentials for Google GMail account.
 * 
 * @see <a href="https://developers.google.com/identity/protocols/OAuth2ServiceAccount#creatinganaccount">Service account -
 *      Overview</a>
 * @see <a href="https://support.google.com/cloud/answer/6158862#service-accounts">Service account - How to create a key</a>
 */
public class MyGmailCredentials {

    /** Email of the Service Account */
    private static final String SERVICE_ACCOUNT_EMAIL = PropertiesManager.getProperty(MyCredentials.PROP_SERVICE_ACCOUNT_EMAIL);

    /** Path to the Service Account's Private Key file */
    private static final String SERVICE_ACCOUNT_PKCS12_FILE_PATH =
            PropertiesManager.getProperty(MyCredentials.PROP_SERVICE_ACCOUNT_PKCS12_FILE_PATH);

//    /** Administrator user to impersonate. */
//    private static final String ADMIN_USER = PropertiesManager.getProperty("agent.googleApps.service.impersonate.admin");
//
//    /**
//     * Build and returns the OAuth2 credentials authorized with the service accounts
//     * that act on behalf of the given user.
//     *
//     * @return credentials
//     * @throws IOException
//     * @throws GeneralSecurityException
//     */
//    static GoogleCredential buildCredentials() throws GeneralSecurityException, IOException {
//        return buildCredentials(ADMIN_USER, SERVICE_ACCOUNT_EMAIL, SERVICE_ACCOUNT_PKCS12_FILE_PATH);
//    }

    /**
     * Build and returns the OAuth2 credentials authorized with the service accounts
     * that act on behalf of the given user.
     *
     * @param usernameOfTheUserBeingChanged This must the the username of the account being changed
     *            because the username of the administrator account does not work
     *            when trying to change the email settings of another user.
     * @return credentials
     * @throws IOException
     * @throws GeneralSecurityException
     */
    static GoogleCredential buildCredentials(final String usernameOfTheUserBeingChanged)
            throws GeneralSecurityException, IOException {
        return buildCredentials(usernameOfTheUserBeingChanged, SERVICE_ACCOUNT_EMAIL, SERVICE_ACCOUNT_PKCS12_FILE_PATH);
    }

    private static GoogleCredential buildCredentials(final String userEmail, final String serviceAccountEmail,
            final String serviceAccountKeyFilePath) throws GeneralSecurityException, IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        GsonFactory jsonFactory = new GsonFactory();

        PrivateKey privateKey = MyCredentials.convertToPrivateKey(serviceAccountKeyFilePath);

//        System.out.println(userEmail);
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(jsonFactory)
                .setServiceAccountId(serviceAccountEmail)
                .setServiceAccountScopes(Arrays.asList(GmailScopes.GMAIL_SETTINGS_BASIC, GmailScopes.GMAIL_SETTINGS_SHARING))
                .setServiceAccountUser(userEmail == null || userEmail.isEmpty() ? null : userEmail)
                .setServiceAccountPrivateKey(privateKey).build();

        return credential;
    }

}
