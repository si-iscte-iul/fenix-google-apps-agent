package pt.iscte.agent.googleApps.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.Directory.Users.Photos;
import com.google.api.services.admin.directory.model.Alias;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.UserName;
import com.google.api.services.admin.directory.model.UserPhoto;

import pt.iscte.agent.googleApps.dto.GoogleAccountDTO;
import pt.iscte.agent.googleApps.util.UserType;

public class DirectoryService {

    // ------------------------------------------------------------------------
    private static Directory CACHED_INSTANCE;
    private static long ADQUIRED_TIME;

    private static final long ONE_DAY_MS = 86400000;

    /**
     * Check if it has passed more than 1 day.
     *
     * @param adquiredTime
     *            time
     * @return true if it has passed more than 1 day
     */
    private static boolean hasExpired(long adquiredTime) {
        long now = System.currentTimeMillis();
        if (now - adquiredTime > ONE_DAY_MS) {
            return true;
        }
        return false;
    }

    public static synchronized Directory getService() {
        if (hasExpired(ADQUIRED_TIME)) {
            // Recycle
            ADQUIRED_TIME = System.currentTimeMillis();

            CACHED_INSTANCE = getDirectoryService();
            System.out.println("Recycling service DirectoryService. " + new Date());
        }
        return CACHED_INSTANCE;
    }

    // ------------------------------------------------------------------------

    private static final String GMAIL_ENABLED_UNIT = "GmailEnabled";
    private static final String GMAIL_DISABLED_UNIT = "GmailDisabled";

    /**
     * Build and returns a Directory service object authorized with the service accounts
     * that act on behalf of the given user.
     *
     * @return Directory service object that is ready to make requests.
     */
    private static Directory getDirectoryService() {
        GoogleCredential credential;
        try {
            credential = MyCredentials.buildCredentials();
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        HttpTransport httpTransport = new NetHttpTransport();
        GsonFactory jsonFactory = new GsonFactory();

        Directory service =
                new Directory.Builder(httpTransport, jsonFactory, null).setHttpRequestInitializer(credential)
                        .setApplicationName("DirectoryFenixAgent").build();
        return service;
    }

    public static Boolean isGmailBlocked(final String orgUnitPath) {
        if (orgUnitPath.endsWith(GMAIL_DISABLED_UNIT)) {
            return Boolean.TRUE;
        }
        if (orgUnitPath.endsWith(GMAIL_ENABLED_UNIT)) {
            return Boolean.FALSE;
        }
        return null;
    }

    public static String buildOrgUnit(final UserType userType, final Boolean gmailBlocked) {
        return "/" + userType.getOrgUnit() + "/" + getGmailOrgUnit(gmailBlocked);
    }

    private static String getGmailOrgUnit(final Boolean gmailBlocked) {
        if (Boolean.TRUE.equals(gmailBlocked)) {
            return GMAIL_DISABLED_UNIT;
        }
        if (Boolean.FALSE.equals(gmailBlocked)) {
            return GMAIL_ENABLED_UNIT;
        }
        return null;
    }

    public static User findUser(final String username) throws IOException {
        try {
            Directory service = getService();

            Directory.Users request = service.users();

            User result = request.get(username).execute();
            return result;
        } catch (GoogleJsonResponseException e) {
            if (e.getStatusCode() != 404) {
                throw e;
            }
        }

        return null;
    }

    public static User createUser(final GoogleAccountDTO dataNew) throws IOException {
        Directory service = getService();

        Directory.Users request = service.users();

        User userInput = new User();

        UserName name = new UserName();
        name.setGivenName(dataNew.getFirstName());
        name.setFamilyName(dataNew.getLastName());
        userInput.setName(name);

        userInput.setPrimaryEmail(dataNew.getUsername());
        userInput.setPassword(dataNew.getPassword());
        userInput.setOrgUnitPath(dataNew.getOrgUnitPath());

        User result = request.insert(userInput).execute();
        return result;
    }

    public static void deleteAllAlias(final GoogleAccountDTO data) throws IOException {
        deleteAliases(data.getUsername(), data.getAliases());
    }

    public static void deleteAliases(final String username, final List<String> aliases) throws IOException {
        if (aliases == null || aliases.isEmpty()) {
            return;
        }

        Directory service = getService();

        for (String alias : aliases) {
            Directory.Users.Aliases request = service.users().aliases();
            request.delete(username, alias).execute();
        }
    }

    public static void deleteAlias(final String username, final String alias) throws IOException {
        Directory service = getService();

        Directory.Users.Aliases request = service.users().aliases();
        request.delete(username, alias).execute();
    }

    public static void createAlias(final String username, final String alias) throws IOException {
        Directory service = getService();

        Directory.Users.Aliases request = service.users().aliases();

        Alias aliasObj = new Alias();
        aliasObj.setAlias(alias);
        request.insert(username, aliasObj).execute();
    }

    public static void createAliases(final String username, final List<String> aliases) throws IOException {
        if (aliases == null || aliases.isEmpty()) {
            return;
        }

        Directory service = getService();

        for (String alias : aliases) {
            Directory.Users.Aliases request = service.users().aliases();

            Alias aliasObj = new Alias();
            aliasObj.setAlias(alias);
            request.insert(username, aliasObj).execute();
        }
    }

    public static void updateUserFullNameAndOrgUnitPath(final String username, final String firstName, final String lastName,
            final String orgUnitPath) throws IOException {
        Directory service = getService();

        Directory.Users request = service.users();

        User userInput = new User();
        UserName name = new UserName();
        name.setGivenName(firstName);
        name.setFamilyName(lastName);
        userInput.setName(name);
        userInput.setOrgUnitPath(orgUnitPath);

        request.update(username, userInput).execute();
    }

    public static User updateUsername(final String oldUsername, final String newUsername) throws IOException {
        Directory service = getService();

        Directory.Users request = service.users();

        User userInput = new User();
        userInput.setPrimaryEmail(newUsername);

        return request.update(oldUsername, userInput).execute();
    }

    public static void changePassword(final String username, final String password) throws IOException {
        Directory service = getService();

        Directory.Users request = service.users();

        User userInput = new User();
        userInput.setPassword(password);

        request.update(username, userInput).execute();
    }

    public static void suspend(final String username, final Boolean suspendedFlag) throws IOException {
        Directory service = getService();

        Directory.Users request = service.users();

        User userInput = new User();
        userInput.setSuspended(suspendedFlag);

        request.update(username, userInput).execute();
    }

    public static void delete(final String username) throws IOException {
        Directory service = getService();

        Directory.Users request = service.users();

        request.delete(username).execute();
    }

    public static void updatePhoto(final String username, byte[] photoData) throws IOException {
        Directory service = getService();

        UserPhoto userPhoto = new UserPhoto().setPrimaryEmail(username).encodePhotoData(photoData);
        Photos photos = service.users().photos();

        UserPhoto userPhotoResult = photos.patch(username, userPhoto).execute();
        System.out.println("userPhotoResult ->" + userPhotoResult);
    }

    public static byte[] getPhoto(final String username) throws IOException {
        Directory service = getService();

        Photos photos = service.users().photos();

        UserPhoto userPhotoResult = photos.get(username).execute();
        System.out.println("userPhotoResult ->" + userPhotoResult);
        return userPhotoResult.decodePhotoData();
    }
}
