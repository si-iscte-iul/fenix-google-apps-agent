package pt.iscte.agent.googleApps.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.util.Arrays;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.SecurityUtils;
import com.google.api.services.admin.directory.DirectoryScopes;

import pt.iscte.agent.googleApps.util.PropertiesManager;

public class MyCredentials {

    /** Email of the Service Account */
    public static final String PROP_SERVICE_ACCOUNT_EMAIL = "agent.googleApps.service.account.email";
    private static final String SERVICE_ACCOUNT_EMAIL = PropertiesManager.getProperty(PROP_SERVICE_ACCOUNT_EMAIL);

    /** Path to the Service Account's Private Key file */
    public static final String PROP_SERVICE_ACCOUNT_PKCS12_FILE_PATH = "agent.googleApps.service.account.pkcs12.file.path";
    private static final String SERVICE_ACCOUNT_PKCS12_FILE_PATH =
            PropertiesManager.getProperty(PROP_SERVICE_ACCOUNT_PKCS12_FILE_PATH);

    /** Administrator user to impersonate. */
    public static final String PROP_ADMIN_USER = "agent.googleApps.service.impersonate.admin";
    private static final String ADMIN_USER = PropertiesManager.getProperty(PROP_ADMIN_USER);

    /**
     * Build and returns the OAuth2 credentials authorized with the service accounts
     * that act on behalf of the given user.
     *
     * @return Directory service object that is ready to make requests.
     * @throws IOException
     * @throws GeneralSecurityException
     */
    static GoogleCredential buildCredentials() throws GeneralSecurityException, IOException {
        return buildCredentials(ADMIN_USER, SERVICE_ACCOUNT_EMAIL, SERVICE_ACCOUNT_PKCS12_FILE_PATH);
    }

    private static GoogleCredential buildCredentials(final String userEmail, final String serviceAccountEmail,
            final String serviceAccountKeyFilePath) throws GeneralSecurityException, IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        GsonFactory jsonFactory = new GsonFactory();

        PrivateKey privateKey = convertToPrivateKey(serviceAccountKeyFilePath);

        GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(jsonFactory)
                .setServiceAccountId(serviceAccountEmail)
                .setServiceAccountScopes(Arrays.asList(DirectoryScopes.ADMIN_DIRECTORY_USER)).setServiceAccountUser(userEmail)
                .setServiceAccountPrivateKey(privateKey).build();

        return credential;
    }

    public static PrivateKey convertToPrivateKey(String serviceAccountKeyFilePath)
            throws IOException, KeyStoreException, GeneralSecurityException {
        java.io.File fileP12 = new java.io.File(serviceAccountKeyFilePath);

        InputStream inputStream;
        if (!fileP12.exists()) {
            // Try to read the file from the classpath,
            // since the String was not a file in the filesystem
            // it should be a classpath String.
            inputStream = MyGmailCredentials.class.getResourceAsStream(serviceAccountKeyFilePath);
        } else {
            inputStream = new FileInputStream(fileP12);
        }

        // The "inputStream" is closed in the SecurityUtils.loadKeyStore method
        // that is called inside the SecurityUtils.loadPrivateKeyFromKeyStore method. 
        PrivateKey privateKey = SecurityUtils.loadPrivateKeyFromKeyStore(SecurityUtils.getPkcs12KeyStore(), inputStream,
                "notasecret", "privatekey", "notasecret");

        return privateKey;
    }

}
