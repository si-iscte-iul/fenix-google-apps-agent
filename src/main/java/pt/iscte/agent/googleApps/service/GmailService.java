package pt.iscte.agent.googleApps.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.AutoForwarding;
import com.google.api.services.gmail.model.ListSendAsResponse;
import com.google.api.services.gmail.model.SendAs;

import pt.iscte.agent.googleApps.dto.ForwardDTO;
import pt.iscte.agent.googleApps.dto.SendAsDTO;
import pt.iscte.agent.googleApps.util.PropertiesManager;

/**
 * Google Apps email settings client.
 */
public class GmailService {

    // Timeout settings
    private static final int CONNECT_TIMEOUT = PropertiesManager.getIntegerProperty("agent.googleApps.service.connectTimeout");
    private static final int READ_TIMEOUT = PropertiesManager.getIntegerProperty("agent.googleApps.service.readTimeout");

    private Gmail service;

    private GmailService(final Gmail service) {
        this.service = service;
    }

    /**
     * Build and returns a Directory service object authorized with the service accounts
     * that act on behalf of the given user.
     *
     * @return Directory service object that is ready to make requests.
     */
    public static GmailService createGmailService(final String usernameOfTheUserBeingChanged) {
        GoogleCredential credential;
        try {
            credential = MyGmailCredentials.buildCredentials(usernameOfTheUserBeingChanged);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        HttpTransport httpTransport = new NetHttpTransport();
        GsonFactory jsonFactory = new GsonFactory();

        final Gmail service = new Gmail.Builder(httpTransport, jsonFactory, null)
                .setHttpRequestInitializer(setHttpTimeout(credential)).setApplicationName("GmailFenixAgent").build();

        return new GmailService(service);
    }

    private static HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
        return new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest httpRequest) throws IOException {
                requestInitializer.initialize(httpRequest);
                httpRequest.setConnectTimeout(CONNECT_TIMEOUT * 1000);
                httpRequest.setReadTimeout(READ_TIMEOUT * 1000);
            }
        };
    }
    // ------------------------------------------------------------------------

    /**
     * Retrieves mail forwarding settings.
     * 
     * @param username
     *            the username
     */
    public ForwardDTO retrieveForwardingDTO(String username) throws IllegalArgumentException, IOException {
        AutoForwarding autoForwarding = service.users().settings().getAutoForwarding(username).execute();
        if (autoForwarding != null) {
            return new ForwardDTO(username, autoForwarding);
        }

        return null;
    }

    /**
     * Changes forwarding settings.
     * 
     * @param data
     *            forward information.
     */
    public void changeForwardingDTO(ForwardDTO data) throws IllegalArgumentException, MalformedURLException, IOException {
        AutoForwarding param = new AutoForwarding();
        param.setEnabled(data.isEnable());
        param.setEmailAddress(data.getForwardTo());
        param.setDisposition(data.getAction());
        service.users().settings().updateAutoForwarding(data.getUsername(), param).execute();
    }

    public List<SendAsDTO> retrieveSendAsDTO(String username) throws IllegalArgumentException, IOException {
        ListSendAsResponse sendAsList = service.users().settings().sendAs().list(username).execute();
        if (sendAsList != null) {
            if (sendAsList.size() > 0) {
                List<SendAsDTO> result = new ArrayList<SendAsDTO>();
                for (SendAs elem : sendAsList.getSendAs()) {
                    result.add(new SendAsDTO(username, elem));
                }
                return result;
            }
        }

        return null;
    }

    public void createSendAsDTO(SendAsDTO data) throws IllegalArgumentException, MalformedURLException, IOException {
        SendAs param = new SendAs();
        param.setIsDefault(data.isDefault());
        param.setDisplayName(data.getName());
        param.setSendAsEmail(data.getAddress());
        param.setReplyToAddress(data.getReplyTo());
        service.users().settings().sendAs().create(data.getUsername(), param).execute();
    }
//
//    @Override
//    protected void authenticate() {
//        try {
//            GoogleCredential credential = MyCredentials.buildCredentials();
//            this.setOAuth2Credentials(credential);
//        } catch (GeneralSecurityException e) {
//            throw new RuntimeException(e);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }

}