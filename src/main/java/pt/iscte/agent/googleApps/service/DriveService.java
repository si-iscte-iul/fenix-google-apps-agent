package pt.iscte.agent.googleApps.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.common.net.MediaType;

import pt.iscte.agent.googleApps.util.PropertiesManager;

/**
 * This class has the methods needed to manage a Google Drive account.
 * 
 * @see <a href="https://developers.google.com/drive/v3/reference/">Google Drive API<a>
 */
public class DriveService {

    private static final String MIME_TYPE_GOOGLE_APPS_FOLDER = "application/vnd.google-apps.folder";
    public static final String PROP_EXTERNAL_ID = "externalId";
    public static final String PROP_SYNCHRONIZED = "sync";

    // Timeout settings
    private static final int CONNECT_TIMEOUT = PropertiesManager.getIntegerProperty("agent.googleApps.service.connectTimeout");
    private static final int READ_TIMEOUT = PropertiesManager.getIntegerProperty("agent.googleApps.service.readTimeout");

    // ------------------------------------------------------------------------
    private static Drive CACHED_INSTANCE;
    private static long ADQUIRED_TIME;

    private static final long ONE_DAY_MS = 86400000;

    /**
     * Check if it has passed more than 1 day.
     * 
     * @param adquiredTime
     *            time
     * @return true if it has passed more than 1 day
     */
    private static boolean hasExpired(long adquiredTime) {
        long now = System.currentTimeMillis();
        if (now - adquiredTime > ONE_DAY_MS) {
            return true;
        }
        return false;
    }

    public static synchronized Drive getService() {
        if (hasExpired(ADQUIRED_TIME)) {
            // Recycle
            ADQUIRED_TIME = System.currentTimeMillis();

            CACHED_INSTANCE = getDriveService();
            System.out.println("Recycling service DriveService. " + new Date());
        }
        return CACHED_INSTANCE;
    }

    // ------------------------------------------------------------------------

    /**
     * Build and returns a Directory service object authorized with the service accounts
     * that act on behalf of the given user.
     *
     * @return Directory service object that is ready to make requests.
     */
    private static Drive getDriveService() {
        GoogleCredential credential;
        try {
            credential = MyDriveCredentials.buildCredentials();
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        HttpTransport httpTransport = new NetHttpTransport();
        GsonFactory jsonFactory = new GsonFactory();

        Drive service = new Drive.Builder(httpTransport, jsonFactory, null).setHttpRequestInitializer(setHttpTimeout(credential))
                .setApplicationName("DriveFenixAgent").build();
        return service;
    }

    private static HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
        return new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest httpRequest) throws IOException {
                requestInitializer.initialize(httpRequest);
                httpRequest.setConnectTimeout(CONNECT_TIMEOUT * 1000);
                httpRequest.setReadTimeout(READ_TIMEOUT * 1000);
            }
        };
    }

    /**
     * Upload a file to the Google Drive.
     * 
     * @param folderId Google Drive folder ID. It's visible in the URL by clicking the folder in the web browser.
     * @param file file to upload
     * 
     * @throws IOException error uploading the file
     */
    public static String uploadFile(String folderId, java.io.File file) throws IOException {
        String contentType = Files.probeContentType(Paths.get(file.getPath()));
        return uploadFile(folderId, file, MediaType.parse(contentType), (Map<String, String>) null);
    }

    /**
     * Upload a file to the Google Drive.
     * 
     * @param folderId Google Drive folder ID. It's visible in the URL by clicking the folder in the web browser.
     * @param file file to upload
     * @param externalId ID on external system
     * 
     * @throws IOException error uploading the file
     */
    public static String uploadFile(String folderId, java.io.File file, String externalId) throws IOException {
        String contentType = Files.probeContentType(Paths.get(file.getPath()));
        Map<String, String> properties = new HashMap<>();
        properties.put(PROP_EXTERNAL_ID, externalId);
        return uploadFile(folderId, file, MediaType.parse(contentType), properties);
    }

    /**
     * Upload a file to the Google Drive.
     * 
     * @param folderId Google Drive folder ID. It's visible in the URL by clicking the folder in the web browser.
     * @param file file to upload
     * @param properties for instance the object externalId if exists
     * 
     * @throws IOException error uploading the file
     */
    public static String uploadFile(String folderId, java.io.File file, Map<String, String> properties) throws IOException {
        String contentType = Files.probeContentType(Paths.get(file.getPath()));
        return uploadFile(folderId, file, MediaType.parse(contentType), properties);
    }

    /**
     * Upload a file to the Google Drive.
     * 
     * @param folderId Google Drive folder ID. It's visible in the URL by clicking the folder in the web browser.
     * @param file file to upload
     * @param mediaType file content type
     * @param externalId ID on external system
     * 
     * @throws IOException error uploading the file
     */
    public static String uploadFile(String folderId, java.io.File file, MediaType mediaType, String externalId)
            throws IOException {
        Map<String, String> properties = new HashMap<>();
        properties.put(PROP_EXTERNAL_ID, externalId);
        return uploadFile(folderId, file, mediaType, properties);
    }

    /**
     * Upload a file to the Google Drive.
     * 
     * @param folderId Google Drive folder ID. It's visible in the URL by clicking the folder in the web browser.
     * @param file file to upload
     * @param mediaType file content type
     * @param properties for instance the object externalId if exists
     * 
     * @throws IOException error uploading the file
     */
    public static String uploadFile(String folderId, java.io.File file, MediaType mediaType, Map<String, String> properties)
            throws IOException {
        // Build a new authorized API client service.
        Drive service = DriveService.getService();

        File content = new File();
        content.setName(file.getName());
        content.setParents(Arrays.asList(folderId));

        if (properties != null && !properties.isEmpty()) {
            content.setAppProperties(properties);
        }

        // Upload file
        AbstractInputStreamContent mediaContent = new FileContent(mediaType.toString(), file);
        File driveFile = service.files().create(content, mediaContent).setFields("id").execute();
        return driveFile.getId();
    }

    /**
     * Upload a file to the Google Drive.
     * 
     * @param folderId Google Drive folder ID. It's visible in the URL by clicking the folder in the web browser.
     * @param fileName Name of the file
     * @param fileLength Length of the file to upload
     * @param inputStream stream with the data
     * @param mediaType file content type
     * @param externalId ID on external system
     * 
     * @throws IOException error uploading the file
     */
    public static String uploadFile(String folderId, String fileName, long fileLength, InputStream inputStream,
            MediaType mediaType, String externalId) throws IOException {
        Map<String, String> properties = new HashMap<>();
        properties.put(PROP_EXTERNAL_ID, externalId);
        return uploadFile(folderId, fileName, fileLength, inputStream, mediaType, properties);
    }

    /**
     * Upload a file to the Google Drive.
     * 
     * @param folderId Google Drive folder ID. It's visible in the URL by clicking the folder in the web browser.
     * @param fileName Name of the file
     * @param fileLength Length of the file to upload
     * @param inputStream stream with the data
     * @param mediaType file content type
     * @param properties for instance the object externalId if exists
     * 
     * @throws IOException error uploading the file
     */
    public static String uploadFile(String folderId, String fileName, long fileLength, InputStream inputStream,
            MediaType mediaType, Map<String, String> properties) throws IOException {
        // Build a new authorized API client service.
        Drive service = DriveService.getService();

        File content = new File();
        content.setName(fileName);
        content.setParents(Arrays.asList(folderId));

        if (properties != null && !properties.isEmpty()) {
            content.setAppProperties(properties);
        }

        // Upload file
        InputStreamContent mediaContent = new InputStreamContent(mediaType.toString(), inputStream);
        mediaContent.setLength(fileLength);
        File driveFile = service.files().create(content, mediaContent).setFields("id").execute();
        return driveFile.getId();
    }

    public static Long getTotalSpace() throws IOException {
        // Build a new authorized API client service.
        Drive service = DriveService.getService();

        About about = service.about().get().setFields("storageQuota").execute();
        return about.getStorageQuota().getLimit();
    }

    public static long getUsedSpace() throws IOException {
        // Build a new authorized API client service.
        Drive service = DriveService.getService();

        About about = service.about().get().setFields("storageQuota").execute();
        return about.getStorageQuota().getUsage();
    }

    public static Long getFreeSpace() throws IOException {
        // Build a new authorized API client service.
        Drive service = DriveService.getService();

        About about = service.about().get().setFields("storageQuota").execute();
        Long limit = about.getStorageQuota().getLimit();
        Long usage = about.getStorageQuota().getUsage();
        if (limit == null) {
            return null;
        }

        return limit - usage;
    }

    /**
     * Delete a file or folder.
     * 
     * <b>Warning:</b>
     * <ul>
     * <li>Cascade delete</li>
     * <li>Permanent deletes it, does not move into the trash folder</li>
     * </ul>
     * 
     * @param fileOrFolderId file or folder ID to delete permanently
     * @throws IOException
     */
    public static void delete(String fileOrFolderId) throws IOException {
        Drive service = DriveService.getService();
        service.files().delete(fileOrFolderId).execute();
    }

    /**
     * Create a folder in Google Drive.
     * 
     * @param folderName folder name
     * @param parentFolderId Google Drive parent folder ID. Use <code>null</code> store it in the root folder.
     * @return Google Folder ID
     * @throws IOException
     */
    public static String createFolder(String folderName, String parentFolderId) throws IOException {
        return createFolder(folderName, parentFolderId, (Map<String, String>) null);
    }

    /**
     * Create a folder in Google Drive.
     * 
     * @param folderName folder name
     * @param parentFolderId Google Drive parent folder ID. Use <code>null</code> store it in the root folder.
     * @param externalId ID on external system
     * @return Google Folder ID
     * @throws IOException
     */
    public static String createFolder(String folderName, String parentFolderId, String externalId) throws IOException {
        Map<String, String> properties = new HashMap<>();
        properties.put(PROP_EXTERNAL_ID, externalId);
        return createFolder(folderName, parentFolderId, properties);
    }

    /**
     * Create a folder in Google Drive.
     * 
     * @param folderName folder name
     * @param parentFolderId Google Drive parent folder ID. Use <code>null</code> store it in the root folder.
     * @param properties for instance the object externalId if exists
     * @return Google Folder ID
     * @throws IOException
     */
    public static String createFolder(String folderName, String parentFolderId, Map<String, String> properties)
            throws IOException {
        Drive service = DriveService.getService();

        File fileMetadata = new File();
        fileMetadata.setName(folderName);
        fileMetadata.setMimeType(MIME_TYPE_GOOGLE_APPS_FOLDER);
        if (parentFolderId != null) {
            fileMetadata.setParents(Arrays.asList(parentFolderId));
        }
        if (properties != null && !properties.isEmpty()) {
            fileMetadata.setAppProperties(properties);
        }

        File file = service.files().create(fileMetadata).setFields("id").execute();
        return file.getId();
    }

    /**
     * Share a file or folder.
     * 
     * @param fileOrFolderId ID of the file/folder to share
     * @param permissionRole writer/reader
     * @param usernames username(s) to share
     * @throws IOException
     * 
     * @see <a href="https://developers.google.com/drive/v3/web/manage-sharing#roles">Valid roles</a>
     */
    public static void shareFileOrFolder(String fileOrFolderId, String permissionRole, String... usernames) throws IOException {
        Drive service = DriveService.getService();

        if (usernames != null) {
            for (String username : usernames) {
                Permission permission = new Permission();
                permission.setEmailAddress(username);
                permission.setType("user");
                permission.setRole(permissionRole);
                service.permissions().create(fileOrFolderId, permission).setSendNotificationEmail(false).execute();
            }
        }
    }

    /**
     * get the web link ot the resource
     * 
     * @param fileOrFolderId ID of the file/folder to share
     * @param permissionRole writer/reader
     * @param usernames username(s) to share
     * @throws IOException
     * 
     * @see <a href="https://developers.google.com/drive/v3/web/manage-sharing#roles">Valid roles</a>
     */
    public static String getWebViewLink(final String fileOrFolderId) throws IOException {
        Drive service = DriveService.getService();

        File file = service.files().get(fileOrFolderId).setFields("webViewLink").execute();
        return file != null ? file.getWebViewLink() : null;
    }

    /**
     * Print information about the current user along with the Drive API
     * settings.
     */
    public static void printAbout() {
        printAbout(true);
    }

    /**
     * Print information about the current user along with the Drive API
     * settings.
     */
    public static void printAbout(boolean humanReadableByteCount) {
        try {
            Drive service = DriveService.getService();
            About about = service.about().get().setFields("storageQuota").execute();

            if (humanReadableByteCount) {
                Long maxUploadSize = about.getMaxUploadSize();
                Long limit = about.getStorageQuota().getLimit();
                Long usage = about.getStorageQuota().getUsage();

                System.out.println("Max upload size: " + humanReadableByteCount(maxUploadSize, false));
                System.out.println("Limit quota: " + humanReadableByteCount(limit, false));
                System.out.println("Used quota: " + humanReadableByteCount(usage, false));
            } else {
                System.out.println("Max upload size (bytes): " + about.getMaxUploadSize());
                System.out.println("Limit quota (bytes): " + about.getStorageQuota().getLimit());
                System.out.println("Used quota (bytes): " + about.getStorageQuota().getUsage());
            }
        } catch (IOException e) {
            System.out.println("An error occurred: " + e);
        }
    }

    public static void printContent() throws IOException {
        printContent(null);
    }

    public static void printContent(String filename) throws IOException {
        List<String> lines = new ArrayList<String>(10000);

        Drive service = DriveService.getService();

        // Print the names and IDs for up to 10 files.
        FileList result = listFiles(service, null);
        List<File> files = result.getFiles();
        if (files == null || files.size() == 0) {
            lines.add("No files found.");
        } else {
            lines.add("Files:");
            do {
                for (File file : files) {
                    List<String> parents = file.getParents();
                    String parentFolders = parents != null ? parents.stream().collect(Collectors.joining("|")) : "";
                    Long size = file.getSize();
                    String sSize = size != null ? humanReadableByteCount(size, false) : "-";
                    lines.add(String.format("%s %s %s %s", parentFolders, file.getId(), sSize, file.getName()));
                }

                String nextPageToken = result.getNextPageToken();
                if (nextPageToken != null) {
                    result = listFiles(service, nextPageToken);
                    files = result.getFiles();
                } else {
                    files = Collections.emptyList();
                }
            } while (files.size() != 0);
        }

        if (filename != null) {
            Files.write(Paths.get(filename), lines);
        } else {
            for (String line : lines) {
                System.out.println(line);
            }
        }

    }

    private static FileList listFiles(Drive service, String pageToken) throws IOException {
        Drive.Files.List fileList =
                service.files().list().setPageSize(1000).setFields("nextPageToken, files(id, name, parents, size)");
        if (pageToken != null) {
            fileList.setPageToken(pageToken);
        }
        return fileList.execute();
    }

    public static List<String> searchFolders(String parentFolderId, LocalDateTime modifiedTime, Boolean before)
            throws IOException {
        Drive service = DriveService.getService();

        List<String> fileIds = new ArrayList<String>();

        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append("'");
        sbQuery.append(parentFolderId);
        sbQuery.append("' in parents");

        sbQuery.append(" and ");
        sbQuery.append("mimeType='");
        sbQuery.append(MIME_TYPE_GOOGLE_APPS_FOLDER);
        sbQuery.append("'");

        if (modifiedTime != null && before != null) {
            sbQuery.append(" and ");
            sbQuery.append("modifiedTime ");
            sbQuery.append(before ? "<" : ">");
            sbQuery.append(" '");
            sbQuery.append(modifiedTime.toString());
            sbQuery.append("'");
        }

        String pageToken = null;
        do {
            FileList result = service.files().list().setQ(sbQuery.toString()).setSpaces("drive")
                    .setFields("nextPageToken, files(id, name, parents)").setPageToken(pageToken).execute();
            for (File file : result.getFiles()) {
                fileIds.add(file.getId());
            }
            pageToken = result.getNextPageToken();
        } while (pageToken != null);

        return fileIds;
    }

    /**
     * Search for synchronized folders.
     * 
     * @param parentFolderId
     * @param returnExternalId if true, returns the externalId instead of the fileId
     * @return fileId or externalId
     * @throws IOException
     */
    public static List<String> searchSynchronizedFolders(String parentFolderId, boolean returnExternalId) throws IOException {
        Drive service = DriveService.getService();

        List<String> fileIds = new ArrayList<String>();

        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append("'");
        sbQuery.append(parentFolderId);
        sbQuery.append("' in parents");

        sbQuery.append(" and ");
        sbQuery.append("mimeType='");
        sbQuery.append(MIME_TYPE_GOOGLE_APPS_FOLDER);
        sbQuery.append("'");

        sbQuery.append(" and ");
        sbQuery.append("appProperties ");
        sbQuery.append(" has { key='");
        sbQuery.append(PROP_SYNCHRONIZED);
        sbQuery.append("' and value = 'true'}");

        String pageToken = null;
        do {
            FileList result = service.files().list().setQ(sbQuery.toString()).setSpaces("drive").setPageSize(1000)
                    .setFields("nextPageToken, files(id, name, parents, appProperties)").setPageToken(pageToken).execute();
            for (File file : result.getFiles()) {
                if (returnExternalId) {
                    Map<String, String> properties = file.getAppProperties();
                    if (properties == null) {
                        continue;
                    }
                    String value = properties.get(PROP_EXTERNAL_ID);
                    if (value != null) {
                        // Add file external ID if exists
                        fileIds.add(value);
                    }
                } else {
                    // Add file ID
                    fileIds.add(file.getId());
                }
            }
            pageToken = result.getNextPageToken();
        } while (pageToken != null);

        return fileIds;
    }

    public static void showFolders(String parentFolderId) throws IOException {
        List<String> fileIds = searchFolders(parentFolderId, null, null);
        showFiles(fileIds);
    }

    public static void showFiles(List<String> fileIds) throws IOException {
        showFiles(fileIds.toArray(new String[fileIds.size()]));
    }

    public static void showFiles(String... fileIds) throws IOException {
        Drive service = DriveService.getService();
        for (String fileId : fileIds) {
            File file = service.files().get(fileId).setFields("id, name, parents").execute();
            System.out.printf("Found file: %s ID(%s) PARENTS«%s»\n", file.getName(), file.getId(), file.getParents());
        }
    }

    /**
     * Find an unique folder inside a parent folder, that has the externalId property with the supplied value.
     * 
     * @param parentFolderId
     * @param name unique name in parent folder
     * @return folderId
     * @throws IOException
     * @throws DuplicateFileFoundException
     * 
     * @see https://developers.google.com/drive/v3/web/search-parameters
     */
    public static String findFolderByName(String parentFolderId, String name) throws IOException, DuplicateFileFoundException {
        return find(parentFolderId, MIME_TYPE_GOOGLE_APPS_FOLDER, false, name, null);
    }

    /**
     * Find an unique folder inside a parent folder, that has the externalId property with the supplied value.
     * 
     * @param parentFolderId
     * @param externalId unique external ID in parent folder
     * @return folderId
     * @throws IOException
     * @throws DuplicateFileFoundException
     * 
     * @see https://developers.google.com/drive/v3/web/search-parameters
     */
    public static String findFolderByExternalId(String parentFolderId, String externalId)
            throws IOException, DuplicateFileFoundException {
        return find(parentFolderId, MIME_TYPE_GOOGLE_APPS_FOLDER, false, null, externalId);
    }

    /**
     * Find an unique file inside a parent folder, that has the externalId property with the supplied value.
     * 
     * @param parentFolderId
     * @param name unique name in parent folder
     * @return folderId
     * @throws IOException
     * @throws DuplicateFileFoundException
     * 
     * @see https://developers.google.com/drive/v3/web/search-parameters
     */
    public static String findFileByName(String parentFolderId, String name) throws IOException, DuplicateFileFoundException {
        return find(parentFolderId, MIME_TYPE_GOOGLE_APPS_FOLDER, true, name, null);
    }

    /**
     * Find an unique file inside a parent folder, that has the externalId property with the supplied value.
     * 
     * @param parentFolderId
     * @param externalId unique external ID in parent folder
     * @return folderId
     * @throws IOException
     * @throws DuplicateFileFoundException
     * 
     * @see https://developers.google.com/drive/v3/web/search-parameters
     */
    public static String findFileByExternalId(String parentFolderId, String externalId)
            throws IOException, DuplicateFileFoundException {
        return find(parentFolderId, MIME_TYPE_GOOGLE_APPS_FOLDER, true, null, externalId);
    }

    /**
     * Find an unique file or folder inside a parent folder, that has the externalId property with the supplied value.
     * 
     * @param parentFolderId
     * @param name unique name in parent folder
     * @param externalId unique external ID in parent folder
     * @return folderId
     * @throws IOException
     * 
     * @see https://developers.google.com/drive/v3/web/search-parameters
     */
    private static String find(String parentFolderId, String mimeContentType, boolean excludeMimeType, String name,
            String externalId) throws IOException, DuplicateFileFoundException {
        Drive service = DriveService.getService();

        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append("'");
        sbQuery.append(parentFolderId);
        sbQuery.append("' in parents");

        if (mimeContentType != null) {
            sbQuery.append(" and ");
            if (excludeMimeType) {
                sbQuery.append("not ");
            }
            sbQuery.append("mimeType='");
            sbQuery.append(mimeContentType);
            sbQuery.append("'");
        }

        if (name != null) {
            sbQuery.append(" and ");
            sbQuery.append("name = '");
            sbQuery.append(name);
            sbQuery.append("'");
        }

        if (externalId != null) {
            sbQuery.append(" and ");
            sbQuery.append("appProperties has { key='");
            sbQuery.append(PROP_EXTERNAL_ID);
            sbQuery.append("' and value = '");
            sbQuery.append(externalId);
            sbQuery.append("'}");
        }

        FileList result = service.files().list().setQ(sbQuery.toString()).setSpaces("drive").setFields("files(id, name, parents)")
                .execute();
        final int size = result.getFiles().size();
        if (size == 0) {
            return null;
        }
        if (size > 1) {
            StringBuilder sb = new StringBuilder();
            for (File file : result.getFiles()) {
                sb.append("Name:").append(file.getName());
                sb.append(" ");
                sb.append("ID:").append(file.getId());
                sb.append(" ");
                sb.append("PARENTS:").append(file.getParents());
                sb.append("|");
            }
            throw new DuplicateFileFoundException("Several files/folders found: " + sb.toString());
        }

        return result.getFiles().get(0).getId();
    }

    public static void emptyTrash() throws IOException {
        Drive service = DriveService.getService();
        service.files().emptyTrash().executeUnparsed();
    }

    public static String humanReadableByteCount(Long bytes, boolean si) {
        if (bytes == null) {
            return "";
        }

        int unit = si ? 1000 : 1024;
        if (bytes < unit)
            return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    /**
     * Mask a folder as fully synchronized.
     * 
     * @param folderId folder ID to mark as fully synchronized.
     * @throws IOException
     */
    public static void markFolderAsSynchronized(String folderId) throws IOException {
        Drive service = DriveService.getService();
        File content = new File();
        Map<String, String> properties = new HashMap<>();
        properties.put(PROP_SYNCHRONIZED, "true");
        content.setAppProperties(properties);
        service.files().update(folderId, content).execute();
    }
}
