package pt.iscte.agent.googleApps.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.Arrays;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.DriveScopes;

import pt.iscte.agent.googleApps.util.PropertiesManager;

/**
 * Credentials for Google Drive account.
 * 
 * @see <a href="https://developers.google.com/identity/protocols/OAuth2ServiceAccount#creatinganaccount">Service account -
 *      Overview</a>
 * @see <a href="https://support.google.com/cloud/answer/6158862#service-accounts">Service account - How to create a key</a>
 */
public class MyDriveCredentials {

    /** Email of the Service Account */
    public static final String PROP_SERVICE_ACCOUNT_EMAIL = "agent.googleApps.service.account.email.drive";
    private static final String SERVICE_ACCOUNT_EMAIL = PropertiesManager.getProperty(PROP_SERVICE_ACCOUNT_EMAIL);

    /** Path to the Service Account's Private Key file */
    public static final String PROP_SERVICE_ACCOUNT_PKCS12_FILE_PATH = "agent.googleApps.service.account.pkcs12.file.path.drive";
    private static final String SERVICE_ACCOUNT_PKCS12_FILE_PATH =
            PropertiesManager.getProperty(PROP_SERVICE_ACCOUNT_PKCS12_FILE_PATH);

    /** Administrator user to impersonate. */
    public static final String PROP_ADMIN_USER = "agent.googleApps.service.impersonate.admin.drive";
    private static final String ADMIN_USER = PropertiesManager.getProperty(PROP_ADMIN_USER);

    /**
     * Build and returns the OAuth2 credentials authorized with the service accounts
     * that act on behalf of the given user.
     *
     * @return Directory service object that is ready to make requests.
     * @throws IOException
     * @throws GeneralSecurityException
     */
    static GoogleCredential buildCredentials() throws GeneralSecurityException, IOException {
        return buildCredentials(ADMIN_USER, SERVICE_ACCOUNT_EMAIL, SERVICE_ACCOUNT_PKCS12_FILE_PATH);
    }

    private static GoogleCredential buildCredentials(final String userEmail, final String serviceAccountEmail,
            final String serviceAccountKeyFilePath) throws GeneralSecurityException, IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        GsonFactory jsonFactory = new GsonFactory();

        PrivateKey privateKey = MyCredentials.convertToPrivateKey(serviceAccountKeyFilePath);

        GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(jsonFactory)
                .setServiceAccountId(serviceAccountEmail).setServiceAccountScopes(Arrays.asList(DriveScopes.DRIVE))
                .setServiceAccountUser(userEmail == null || userEmail.isEmpty() ? null : userEmail)
                .setServiceAccountPrivateKey(privateKey).build();

        return credential;
    }

}
