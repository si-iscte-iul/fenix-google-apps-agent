package pt.iscte.agent.googleApps.service;

import com.google.gdata.client.appsforyourdomain.UserService;
import com.google.gdata.util.AuthenticationException;

/**
 * Since I didn't found a new way to check user the user credentials, I left the code in this separate class.
 */
public class AuthenticationService {

    /**
     * Checks if a user can successfully log into Google Apps.
     * 
     * @param userEmail
     *            The user e-mail address.
     * @param userPassword
     *            The user password.
     * @throws AuthenticationException
     *             In case the user credentials are incorrect, or we were
     *             introduced with CAPTCHA.
     */
    public static void checkCredentials(final String userEmail, final String userPassword) throws AuthenticationException {
        UserService oridinaryUserService = new UserService("TestAuthentication");
        oridinaryUserService.setUserCredentials(userEmail, userPassword);
    }

}
