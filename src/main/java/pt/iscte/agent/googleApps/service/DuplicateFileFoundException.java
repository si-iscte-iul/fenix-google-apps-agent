package pt.iscte.agent.googleApps.service;

public class DuplicateFileFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public DuplicateFileFoundException(String message) {
        super(message);
    }
}
