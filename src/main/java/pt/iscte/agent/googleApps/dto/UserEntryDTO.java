package pt.iscte.agent.googleApps.dto;

import com.google.api.services.admin.directory.model.User;

public class UserEntryDTO {

    private String primaryEmail;
    private String password;
    private String firstName;
    private String lastName;
    private boolean isSuspended;

    public UserEntryDTO(String firstName, String lastName) {
        setFirstName(firstName);
        setLastName(lastName);
    }

    public UserEntryDTO(User user) {
        primaryEmail = user.getPrimaryEmail();
        password = user.getPassword();
        if (user.getName() != null) {
            firstName = user.getName().getGivenName();
            lastName = user.getName().getFamilyName();
        } else {
            firstName = null;
            lastName = null;
        }
        setSuspended(user.getSuspended());
    }

    public String getPrimaryEmail() {
        return primaryEmail;
    }

    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSuspended(boolean isSuspended) {
        this.isSuspended = isSuspended;
    }

    public boolean isSuspended() {
        return isSuspended;
    }

}
