package pt.iscte.agent.googleApps.dto;

import com.google.api.services.gmail.model.SendAs;

public class SendAsDTO {

    private String username;
    private String name; // Support
    private String address; // "support@example.com"
    private String replyTo; // "b.smith@gmail.com"
    private boolean isDefault; // false
    private boolean isVerified; // true

    public SendAsDTO(String username, SendAs data) {
        this(username, data.getSendAsEmail(), data.getDisplayName(), data.getReplyToAddress(),
                Boolean.valueOf(data.getIsDefault()), "accepted".equals(data.getVerificationStatus()));
    }

    public SendAsDTO(String username, String name, String address, String replyTo, boolean isDefault, boolean isVerified) {
        super();
        this.username = username;
        this.name = name;
        this.address = address;
        this.replyTo = replyTo;
        this.isDefault = isDefault;
        this.isVerified = isVerified;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("username: ").append(username);
        sb.append(" | ");
        sb.append("address: ").append(address);
        sb.append(" | ");
        sb.append("name: ").append(name);
        sb.append(" | ");
        sb.append("replyTo: ").append(replyTo);
        sb.append(" | ");
        sb.append("isDefault: ").append(isDefault);
        sb.append(" | ");
        sb.append("isVerified: ").append(isVerified);

        return sb.toString();
    }
}
