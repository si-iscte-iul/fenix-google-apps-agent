package pt.iscte.agent.googleApps.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import pt.iscte.agent.googleApps.util.UserType;

public class GoogleAccountDTO implements Serializable {
    private static String NL = System.getProperty("line.separator");

    private static final long serialVersionUID = 1L;

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String mainAlias;
    private UserType userType;
    private Boolean gmailBlocked;
    private ArrayList<String> aliases;
    private String orgUnitPath;
    private GoogleAccountForwardDTO forwardInfo;

    public GoogleAccountDTO() {
        super();
        this.aliases = new ArrayList<String>();
    }

    public GoogleAccountDTO(final GoogleAccountDTO param, final UserType userType, final String orgUnitPath) {
        this(param.getUsername(), param.getPassword(), param.getFirstName(), param.getLastName(), param.getMainAlias(), param
                .getGmailBlocked(), userType, orgUnitPath);
    }

    public GoogleAccountDTO(String username, String password, String firstName, String lastName, String mainAlias,
            Boolean gmailBlocked, UserType userType, String orgUnitPath) {
        this();
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mainAlias = mainAlias;
        this.gmailBlocked = gmailBlocked;
        this.userType = userType;
        this.orgUnitPath = orgUnitPath;
    }

    public ArrayList<String> getAliases() {
        return aliases;
    }

    public void addAlias(String alias) {
        if (alias == null) {
            return;
        }
        this.aliases.add(alias);
    }

    // Required to be the getter be on the generated WS client
    public void setAliases(ArrayList<String> aliases) {
        this.aliases = aliases;
    }

    public void addAlias(Collection<String> alias) {
        if (alias == null) {
            return;
        }
        this.aliases.addAll(alias);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMainAlias() {
        return mainAlias;
    }

    public void setMainAlias(String mainAlias) {
        this.mainAlias = mainAlias;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Boolean getGmailBlocked() {
        return gmailBlocked;
    }

    public void setGmailBlocked(Boolean gmailBlocked) {
        this.gmailBlocked = gmailBlocked;
    }

    public String getOrgUnitPath() {
        return orgUnitPath;
    }

    public void setOrgUnitPath(String orgUnitPath) {
        this.orgUnitPath = orgUnitPath;
    }

    public GoogleAccountForwardDTO getForwardInfo() {
        return forwardInfo;
    }

    public void setForwardInfo(GoogleAccountForwardDTO forwardInfo) {
        this.forwardInfo = forwardInfo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("FirstName: ").append(getFirstName()).append(NL);
        sb.append("LastName: ").append(getLastName()).append(NL);
        sb.append("MainAlias: ").append(getMainAlias()).append(NL);
        sb.append("Password: ").append(getPassword()).append(NL);
        sb.append("Username: ").append(getUsername()).append(NL);
        sb.append("GmailBlocked: ").append(getGmailBlocked()).append(NL);
        sb.append("UserType: ").append(getUserType()).append(NL);
        sb.append("OrgUnitPath: ").append(getOrgUnitPath()).append(NL);

        StringBuilder sbAux = new StringBuilder();
        for (String alias : aliases) {
            sbAux.append("|").append(alias);
        }
        if (sbAux.length() > 0) {
            sbAux.deleteCharAt(0);
        }
        sb.append("Alias: " + sbAux.toString()).append(NL);

        sb.append("Forward: " + (forwardInfo != null ? forwardInfo : "None"));

        return sb.toString();
    }

}
