package pt.iscte.agent.googleApps.dto;

import com.google.api.services.gmail.model.AutoForwarding;

public class ForwardDTO {

    private String username;
    private boolean enable;
    private String forwardTo;
    private String action;

    public ForwardDTO(String username, AutoForwarding data) {
        this(username, data.getEnabled(), data.getEmailAddress(), data.getDisposition());
    }

    public ForwardDTO(String username, boolean enable, String forwardTo, String action) {
        this.username = username;
        this.enable = enable;
        this.forwardTo = forwardTo;
        this.action = action;
    }

    public String getUsername() {
        return username;
    }

    public boolean isEnable() {
        return enable;
    }

    public String getForwardTo() {
        return forwardTo;
    }

    public String getAction() {
        return action;
    }

}
