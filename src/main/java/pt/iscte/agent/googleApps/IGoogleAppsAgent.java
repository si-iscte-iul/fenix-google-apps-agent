package pt.iscte.agent.googleApps;

import pt.iscte.agent.googleApps.dto.GoogleAccountDTO;

public interface IGoogleAppsAgent {
    public boolean authenticate(String userName, String password);

    public boolean changePassword(String userName, String password);

    public boolean isUserInGoogle(String userName);

    public boolean synchronizeUser(GoogleAccountDTO data);

    public boolean synchronizeSendAs(GoogleAccountDTO data);

    public GoogleAccountDTO getUserInfo(String userName);

    public boolean disableAccount(String userName);

    public boolean enableAccount(String userName);

    public boolean deleteAccount(String userName);

}
