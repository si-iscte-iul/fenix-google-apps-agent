package pt.iscte.agent.googleApps.test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.DirectoryScopes;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.Users;

public class DirectoryCommandLine {

    /** Email of the Service Account */
    private static final String SERVICE_ACCOUNT_EMAIL =
            "419481135642-vvbplb5obflfl9adr06k26o2dgbirhlg@developer.gserviceaccount.com";

    /** Path to the Service Account's Private Key file */
    private static final String SERVICE_ACCOUNT_PKCS12_FILE_PATH =
            "/ssd-drive/home/ISCTE/ajsco/projects/GoogleAppsAgentNew/key/fenix-agent-test-1d61a99ea9d6.p12";

    private static final String ADMIN_USER = "ajsco@iscte.pt";

    /**
     * Build and returns a Directory service object authorized with the service accounts
     * that act on behalf of the <i>ADMIN_USER</i>.
     *
     * @return Directory service object that is ready to make requests.
     */
    public static Directory getDirectoryService() throws GeneralSecurityException, IOException, URISyntaxException {
        return getDirectoryService(ADMIN_USER);
    }

    /**
     * Build and returns a Directory service object authorized with the service accounts
     * that act on behalf of the given user.
     *
     * @param userEmail The email of the user. Needs permissions to access the Admin APIs.
     * @return Directory service object that is ready to make requests.
     */
    private static Directory getDirectoryService(String userEmail) throws GeneralSecurityException, IOException,
            URISyntaxException {
        HttpTransport httpTransport = new NetHttpTransport();
        GsonFactory jsonFactory = new GsonFactory();
        GoogleCredential credential =
                new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(jsonFactory)
                        .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
                        .setServiceAccountScopes(Arrays.asList(DirectoryScopes.ADMIN_DIRECTORY_USER))
                        .setServiceAccountUser(userEmail)
                        .setServiceAccountPrivateKeyFromP12File(new java.io.File(SERVICE_ACCOUNT_PKCS12_FILE_PATH)).build();
        Directory service =
                new Directory.Builder(httpTransport, jsonFactory, null).setHttpRequestInitializer(credential)
                        .setApplicationName("DirectoryCommandLine").build();
        return service;
    }

    public static void main(String[] args) throws Exception {
        // Create a new authorized API client
        Directory service = getDirectoryService();

        List<User> allUsers = new ArrayList<User>();
//        Directory.Users.List request = service.users().list().setCustomer("my_customer");
        Directory.Users.List request = service.users().list().setDomain("iscte.pt");
//        Directory.Users.List request = service.users().list().setCustomer("my_customer");

        // Get all users
        do {
            try {
                Users currentPage = request.execute();
                allUsers.addAll(currentPage.getUsers());
                request.setPageToken(currentPage.getNextPageToken());
            } catch (IOException e) {
                System.out.println("An error occurred: " + e);
                request.setPageToken(null);
            }
        } while (request.getPageToken() != null && request.getPageToken().length() > 0);

        // Print all users
        for (User currentUser : allUsers) {
            System.out.println(currentUser.getPrimaryEmail());
        }
    }
}