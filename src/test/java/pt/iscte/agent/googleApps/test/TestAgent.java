package pt.iscte.agent.googleApps.test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import pt.iscte.agent.googleApps.GoogleAppsAgent;
import pt.iscte.agent.googleApps.dto.GoogleAccountDTO;
import pt.iscte.agent.googleApps.service.DirectoryService;
import pt.iscte.agent.googleApps.util.PropertiesManager;

public class TestAgent {
    public static void main(String[] args) throws Exception {
        PropertiesManager.init(new PropertiesManager.DefaultPropertiesResolver());

//        testGetUserInfo("a26000@iscte.pt");
//        
//        testGetUserInfo("ajsco@iscte.pt");

//        testChangePassword("bfritat2@iscte.pt", "iscte2015.x");
//        testChangePassword("bfritat2@iscte.pt", "iscte2");

//        testSynchronizeUser();
//        testGetUserInfo("bfritat6@iscte-iul.pt");

//        testChangePassword("bfritat6@iscte.pt", "iscte2015.0");

//        testDisableAccount("bfritat6@iscte.pt");
//        testGetUserInfo("bfritat6@iscte-iul.pt");

//        testEnableAccount("bfritat6@iscte.pt");
//        testGetUserInfo("bfritat6@iscte-iul.pt");

//        testAuthentication("bfritat3@iscte.pt", "iscte2015.");

        String username = "xpto@iscte-iul.pt";
        String firstName = "Primeiro";
        String familyName = "Ultimo";
        String password = "pass";
        String mainAlias = "primeiro.ultimo@iscte-iul.pt";
        Boolean gmailBlocked = true;
        testSynchronizeUser(username, firstName, familyName, password, gmailBlocked, mainAlias);
    }

    private static void testSynchronizeUser(String username, String firstName, String familyName, String password,
            Boolean gmailBlocked, String mainAlias) {
        String usernameForDomain = username.substring(0, username.indexOf("@")) + "@iscte-iul.pt";

        GoogleAppsAgent agente = new GoogleAppsAgent();

        GoogleAccountDTO param = new GoogleAccountDTO();
        param.setUsername(usernameForDomain);
        param.setPassword(password);
        param.setFirstName(firstName);
        param.setLastName(familyName);
        param.setMainAlias(mainAlias);
        param.setGmailBlocked(gmailBlocked);

        agente.synchronizeUser(param);
    }

    private static void testAuthentication(String username, String password) throws Exception {
        GoogleAppsAgent agente = new GoogleAppsAgent();
        boolean success = agente.authenticate(username, password);
        System.out.println(success);
    }

    private static void testDisableAccount(String username) throws Exception {
        GoogleAppsAgent agente = new GoogleAppsAgent();
        boolean success = agente.disableAccount(username);
        System.out.println(success);
    }

    private static void testEnableAccount(String username) throws Exception {
        GoogleAppsAgent agente = new GoogleAppsAgent();
        boolean success = agente.enableAccount(username);
        System.out.println(success);
    }

    private static void testSynchronizeUser() throws Exception {
        {
            String domain = "iscte-iul.pt";

            String usernameWithoutDomain = "bfritat8";
            String username = usernameWithoutDomain + "@" + domain;
            String emailWithoutDomain = "Batata.FritaT8";
            String email = emailWithoutDomain + "@" + domain;

            System.out.println("ANTES");
            testGetUserInfo(username);

            GoogleAccountDTO data = new GoogleAccountDTO();

            data.setUsername(username);
            data.setMainAlias(email);
            data.setLastName("FritaT8");
            data.setFirstName("Batata");
            data.setGmailBlocked(false);
            data.setPassword("iscte2015.");

            testSynchronizeUser(data);

            System.out.println("DEPOIS");
            testGetUserInfo(username);
        }

    }

    private static void testSynchronizeUser(final GoogleAccountDTO data) throws Exception {
        GoogleAppsAgent agente = new GoogleAppsAgent();
        boolean success = agente.synchronizeUser(data);
        System.out.println(success);
    }

    private static void testGetUserInfo(final String username) throws Exception {
        GoogleAppsAgent agente = new GoogleAppsAgent();
        GoogleAccountDTO userInfo = agente.getUserInfo(username);
        System.out.println(userInfo);
    }

    private static void testChangePassword(final String username, final String password) throws Exception {
        GoogleAppsAgent agente = new GoogleAppsAgent();
        boolean success = agente.changePassword(username, password);
        System.out.println(success);
    }

    private static void testePhotoUpload(final String username, final String fileName) throws Exception {
        Path path = Paths.get(fileName);
        byte[] photoData = Files.readAllBytes(path);
        DirectoryService.updatePhoto(username, photoData);
    }

    private static void testeGetPhoto(final String username, final String fileName) throws Exception {
        byte[] data = DirectoryService.getPhoto(username);
        Files.write(new File(fileName).toPath(), data);
    }
}
