package pt.iscte.agent.googleApps.test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import com.google.common.io.Files;

import pt.iscte.agent.googleApps.service.DriveService;
import pt.iscte.agent.googleApps.util.PropertiesManager;

public class DriveTest {

    private static final String[] GDSI_TEAM = { "ajsco@iscte.pt", "pmmsa@iscte.pt", "rgbsa@iscte.pt", "pmpp@iscte.pt",
            "alsl@iscte.pt", "lmcms2@iscte.pt", "mcrgc@iscte.pt" };

    private static final String[] GEAPQ_TEAM = { "rvgis@iscte.pt", "cmcms@iscte.pt", "arrva@iscte.pt" };

    private static final String GOOGLE_DRIVE_SURVEY_FOLDER_ID = "0B6OYig15JHMRVmhKdmRLYlo3bW8";
    
    public static void main(String[] args) throws IOException {
        PropertiesManager.init(new PropertiesManager.DefaultPropertiesResolver());

//        DriveService.uploadFile("0B2hT9XJ2M7XVQ293MXJ0YVc3bEE", new java.io.File("/home/ISCTE/ajsco/Documents/sms-vodafone.pdf"));

//        String folderId = DriveService.createFolder("inq-monit-pedagogica", "0B6OYig15JHMRS1YyZlhiejZGaGs");

//        DriveService.shareFileOrFolder("0B6OYig15JHMRVmhKdmRLYlo3bW8", "reader", GEAPQ_TEAM);

//        init("FENIX-DRIVE-DEV");
//        testMe2();
//
//      String folderId = DriveService.createFolder("ARCHIVE", "0B6OYig15JHMRVmhKdmRLYlo3bW8");
//      DriveService.shareFileOrFolder(folderId, "writer", GDSI_TEAM);
//      String folderId2 = DriveService.createFolder("2019-2020", folderId);
//      DriveService.shareFileOrFolder(folderId2, "writer", GDSI_TEAM);
//      String folderId3 = DriveService.createFolder("2020-2021", folderId);
//      DriveService.shareFileOrFolder(folderId3, "writer", GDSI_TEAM);
//      String folderId4 = DriveService.createFolder("2021-2022", folderId);
//      DriveService.shareFileOrFolder(folderId4, "writer", GDSI_TEAM);
//      String folderId5 = DriveService.createFolder("2022-2023", folderId);
//      DriveService.shareFileOrFolder(folderId5, "writer", GDSI_TEAM);
        
        File sampleFile = File.createTempFile("Batatas", ".txt");
        Files.write("Texto 123".getBytes(), sampleFile);
        
        System.out.println(sampleFile.toString());      
        DriveService.uploadFile(GOOGLE_DRIVE_SURVEY_FOLDER_ID, sampleFile);

//        freeSpaceDeleteFilesInFolder("1WEOEzD4Uje-SRdjH1VDqbDfMC86Diqe9");
        System.out.println("FIM");      
    }

    private static void freeSpaceDeleteFilesInFolder(String folderWithFilesToDelete) throws IOException {
//        DriveService.printAbout();
        
//        moveFiles();

        DriveService.printContent("analisar.txt");

//        System.out.println("Before DELETE");
//        DriveService.printAbout();
//
//        System.out.println("Starting delete");
//        List<String> folderIds = DriveService.searchFolders(folderWithFilesToDelete, LocalDateTime.now(), true);
//        for (String folderId : folderIds) {
//            System.out.println("Deleting " + folderId);
//            DriveService.delete(folderId);
//        }

        System.out.println("After DELETE");
        DriveService.printAbout();
    }

    private static void moveFiles() {
//      String folderId = DriveService.createFolder("cleanup", "1WEOEzD4Uje-SRdjH1VDqbDfMC86Diqe9");
//      DriveService.shareFileOrFolder(folderId, "writer", GDSI_TEAM);
//
//      Drive service = DriveService.getService();
//
//      String folderId = "18NaVQjhoM_bWG1FM1M_q8HER2t5a5z-c";
//      List<String> filesId =
//              // "0B6OYig15JHMRMGtleHZQMUZFUjA", 
//              Arrays.asList("0B6OYig15JHMRS3c2VzhYS200M1U",
//                      "0B6OYig15JHMRX0pkUDdpSDRJT3c", "0B6OYig15JHMRVXpkV3N3UDd1dk0", "0B6OYig15JHMRa3VyWjVJb0ttUFU",
//                      "0B6OYig15JHMRMS03TDEyOHpzNDg", "0B6OYig15JHMRS21adkpvcXZ0cWc", "0B6OYig15JHMRR1lvUE5WX3U2Tk0",
//                      "0B6OYig15JHMRWU96bE5ZVzI2Q2s", "0B6OYig15JHMRaVZaYVJvOU80eVE", "0B6OYig15JHMReTg0MFZ2VTh3eVE",
//                      "0B6OYig15JHMRZTlWckI5YlRvNVE", "0B6OYig15JHMRUDd4TVJoR21HNjQ", "0B6OYig15JHMRbkx6c1B5RUhsbWs",
//                      "0B6OYig15JHMRM2RCS2g3Ty16Q0E", "0B6OYig15JHMRNnZqSVhDNk1pSnM", "0B6OYig15JHMRNVZ1SzBLQXFpTEU",
//                      "0B6OYig15JHMRMTBNTFVfQTY1WHM", "0B6OYig15JHMRR3FjU01veDlXZHM", "0B6OYig15JHMRWEJYYUtHdllMYXM",
//                      "0B6OYig15JHMRUUdNY2pfRGhCajA", "0B6OYig15JHMRZk1Na0VBUVdybjA", "0B6OYig15JHMRaW02emlyYlI2OW8",
//                      "0B6OYig15JHMROENQYnUtZjkwYzQ", "0B6OYig15JHMRd0x4M3pfZ1dzbm8", "0B6OYig15JHMRVGYyV1ZQQzIyWG8",
//                      "0B6OYig15JHMRNDVDQVRQelY3V00", "0B6OYig15JHMRUkpvampLbHcyS2M", "0B6OYig15JHMRaTctS2lhLW9BMlk",
//                      "0B6OYig15JHMRWWs2MGhXZ2FzOWM", "0B6OYig15JHMRVXd5ZEFFekdhOHc", "0B6OYig15JHMRbXU4UUpYMXFidUk",
//                      "0B6OYig15JHMRdTAySG1zeE5NVEk", "0B6OYig15JHMRNzNYRXJNVUo4UVU", "0B6OYig15JHMRLWVjUnhFT1IzQm8",
//                      "0B6OYig15JHMRODVMRDF1d1RFYW8", "0B6OYig15JHMRWTVPTlZEVE9qd0k", "0B6OYig15JHMRNzVpcHdMRHdscXM",
//                      "0B6OYig15JHMRYWN1SXRZdzBDdzA", "0B6OYig15JHMRenhGSXkwM01YbEk", "0B6OYig15JHMRWjVPNVRpQldmelE",
//                      "0B6OYig15JHMRTDRfVE1ZeTJwTHM", "0B6OYig15JHMRaHY4UnBFZjREdTg", "0B6OYig15JHMRUzZmYWVMOS1FZk0",
//                      "0B6OYig15JHMRdlUybWJXN3llVlk", "0B6OYig15JHMRZVROV1RyWXVHMUU", "0B6OYig15JHMRQWZRRVV3dzlkYmc",
//                      "0B6OYig15JHMRSjFtQkhPb2JLYUE", "0B6OYig15JHMRMWlfc2w2amRNLVU", "0B6OYig15JHMRaTNPdloyMEQwclk",
//                      "0B6OYig15JHMROG02a3BBbkRvd0k", "0B6OYig15JHMRMFRrUkxQRzJsd1E", "0B6OYig15JHMRZjdzS3lKbHlQZEE",
//                      "0B6OYig15JHMRYmREMnkxWndVb2c", "0B6OYig15JHMRM1FvanVlMGwzM1E", "0B6OYig15JHMROGlzdWRTcjlDT3M",
//                      "0B6OYig15JHMROXEwREtpeGR1Sk0", "0B6OYig15JHMRczV6cE8zaFhrM3M", "0B6OYig15JHMRd202MjNfbkNHNEU",
//                      "0B6OYig15JHMRUElJYVQxVHA0b28", "0B6OYig15JHMRd0hKZF9YYmVYN2c", "0B6OYig15JHMRWHhCMUJTbWg2R1U",
//                      "0B6OYig15JHMRSnVoX2RaM29oTkE", "0B6OYig15JHMRaVo4b0Jmdy10T0U", "0B6OYig15JHMRUGtadUlRUzltczQ",
//                      "0B6OYig15JHMRLURldzNwZkI1ZjA", "0B6OYig15JHMRRkVqM1BCaVppUFU", "0B6OYig15JHMRdWd4SzVaV2xpSUE",
//                      "0B6OYig15JHMRcnlsTVd0WjREVFU", "0B6OYig15JHMRRmRsSVhwYkdJSUk", "0B6OYig15JHMRNEFXRF80NF9kdHM",
//                      "0B6OYig15JHMRbG9YYVZVeXU5aWM", "0B6OYig15JHMRbXhLTDVSM0RpWTQ", "0B6OYig15JHMRRm1nWHRHbnd0OTg",
//                      "0B6OYig15JHMReHhDOUk1eG5CeWs", "0B6OYig15JHMRbmtDRHlnQVIyZGc", "0B6OYig15JHMRSW1TbS1FWVdUd00",
//                      "0B6OYig15JHMRbTI2YUhtU1ZRWUE", "0B6OYig15JHMRaWRUX2pBVnl1YUE", "0B6OYig15JHMRbnk0R0dMYThoVGM",
//                      "0B6OYig15JHMRVzIxSXJ6Skc5bEE", "0B6OYig15JHMRQVBPeWg1eWgtcHc", "0B6OYig15JHMRbXJBbDdZUTFKMTg",
//                      "0B6OYig15JHMRckZBbmdMY2lKYTA", "0B6OYig15JHMRc0c1ZkprbFRpNkU", "0B6OYig15JHMRVnF4WGhLbUVOTEk",
//                      "0B6OYig15JHMRR2RsdTBBMDFLZXM", "0B6OYig15JHMRQTJNakdBTldGc1k", "0B6OYig15JHMRUWg2S214U2pkZ3c",
//                      "0B6OYig15JHMRR1JVUmhNNVhWcTA", "0B6OYig15JHMROVdkQjVsQ3Q4RU0", "0B6OYig15JHMRckZkU3R4VDh1Qk0",
//                      "0B6OYig15JHMRX0pBa0FjVGtPcVU", "0B6OYig15JHMReFpLWF9Zc2VSclU", "0B6OYig15JHMRWXdMMURzcGZyRk0",
//                      "0B6OYig15JHMRUTZhYkptU1FvNnc", "0B6OYig15JHMReVJ2c09mYm96NzA", "0B6OYig15JHMRV3BHQnM5X28zaEk",
//                      "0B6OYig15JHMRTjRVbUEtcWtQS0U", "0B6OYig15JHMROVMtMWdGdHBMa2M", "0B6OYig15JHMRYTdVZXZvbFVPbDg",
//                      "0B6OYig15JHMRelI2WjhqdUdMQjQ", "0B6OYig15JHMRVkZaYk85SDZfNGs", "0B6OYig15JHMRM1FvenRDV1VYeGc",
//                      "0B6OYig15JHMReGU4QTR3RkFKb1k", "0B6OYig15JHMRVjI5bU9YWDJPd1E", "0B6OYig15JHMRaG9rQTdYelhTMjA",
//                      "0B6OYig15JHMRNXZGa0xZZWdyalE", "0B6OYig15JHMRTDZfZ3ZSVVNYZ0E", "0B6OYig15JHMRbjNkeG04VW1YNnM",
//                      "0B6OYig15JHMRX3FfcXVJYVdySXM", "0B6OYig15JHMRbjBIRjBHWXl2UUk", "0B6OYig15JHMRTF9vb0dlWE5Sb28");
//      for (String fileId : filesId) {
//          File file = service.files().update(fileId, null).setAddParents(folderId).setFields("id, parents").execute();
//      }
    }

    private static void init(String rootFolderName) throws IOException {
        String rootFolderId = DriveService.createFolder(rootFolderName, null);
        DriveService.createFolder("archive", rootFolderId);
        DriveService.createFolder("current", rootFolderId);
        DriveService.createFolder("temp", rootFolderId);

        DriveService.shareFileOrFolder(rootFolderId, "writer", "ajsco@iscte.pt");
    }

    private static void testMe2() throws IOException {
        String folderId = DriveService.createFolder("test123", "0B6OYig15JHMRWWgtd1lBUUdlaGc");
        System.out.println(folderId);
//        DriveService.shareFileOrFolder(folderId, "reader", "ajsco@iscte.pt", "pmpp@iscte.pt");

//        DriveService.printContent();
//        DriveService.printAbout();
//        DriveService.searchFolders();
//        DriveService.emptyTrash();
        DriveService.printAbout();

//        DriveService.uploadFile("0ByVqQ55Vf1H9b3M3b3R5dXBHdzQ", new java.io.File("/home/ISCTE/ajsco/Documents/sms-vodafone.pdf"),
//                "163209059780");

//        DriveService.markFolderAsSynchronized("0ByVqQ55Vf1H9cjdnd1ctTEItWTQ");
//        DriveService.markFolderAsSynchronized("0ByVqQ55Vf1H9NlhwZ1FlT21Lckk");
//        DriveService.markFolderAsSynchronized("0ByVqQ55Vf1H9Nlo5WWhKeThBQUU");

//        String folderId = DriveService.createFolder("batataFrita", "0ByVqQ55Vf1H9b3M3b3R5dXBHdzQ", "myEid");

//        Drive service = DriveService.getService();

//        File myFile = service.files().get("0ByVqQ55Vf1H9Nlo5WWhKeThBQUU").setFields("appProperties").execute();

//        System.out.println("1222");

//        List<String> fileIds = DriveService.searchSynchronizedFolders("0B6OYig15JHMRTUl1cnFZQjFKTTg", true);
        List<String> fileIds = DriveService.searchFolders("0B6OYig15JHMRWWgtd1lBUUdlaGc", null, null);
        DriveService.showFiles(fileIds);
    }

}